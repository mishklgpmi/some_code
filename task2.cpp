#include <stdexcept>
#include <iostream>

enum class Lang
{
    CPP,
    JAVA,
    PHP,
    JS
};

class CodeGenerator
{
public:
    CodeGenerator() {}
    virtual ~CodeGenerator() {}
    std::string generateCode()
    {
        return "Code = " + someCodeRelatedThing();
    };

protected:
    virtual std::string someCodeRelatedThing() = 0;
};

class CppCodeCenerator : public CodeGenerator
{
public:
    CppCodeCenerator() : CodeGenerator() {}
    virtual ~CppCodeCenerator() {}

protected:
    std::string someCodeRelatedThing() override
    {
        return "CPP code";
    }
};

class PhpCodeCenerator : public CodeGenerator
{
public:
    PhpCodeCenerator() : CodeGenerator() {}
    virtual ~PhpCodeCenerator() {}

protected:
    std::string someCodeRelatedThing() override
    {
        return "PHP code";
    }
};

class JavaCodeCenerator : public CodeGenerator
{
public:
    JavaCodeCenerator() : CodeGenerator() {}
    virtual ~JavaCodeCenerator() {}

protected:
    std::string someCodeRelatedThing() override
    {
        return "JAVA code";
    }
};

std::shared_ptr<CodeGenerator> codeGeneratorFactory(enum class Lang lang)
{
    switch(lang)
    {
        case Lang::CPP: return std::make_shared<CppCodeCenerator>();
        case Lang::JAVA: return std::make_shared<JavaCodeCenerator>();
        case Lang::PHP: return std::make_shared<PhpCodeCenerator>();
    }
    throw std::logic_error("Bad language");
}

int main()
{
    try {
        std::shared_ptr<CodeGenerator> gen = codeGeneratorFactory(Lang::JS);
        std::cout << gen->generateCode() << std::endl;
    }
    catch(std::logic_error& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
