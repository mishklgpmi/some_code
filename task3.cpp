#include <iostream>
#include <fstream>

// Agreement для всего проекта
// размер идентификатора Feature = 4 байта
constexpr uint32_t eCircle = 1;
constexpr uint32_t eTriangle = 2;
constexpr uint32_t eSquare = 3;
// и т.д.

class Shape
{
public:
    Shape(int n) : m_n(n) {}
    virtual ~Shape()
    {
        if(m_points != nullptr)
            delete[] m_points;
    }
    bool Read(std::ifstream& in)
    {
        try {
            m_points = new double[m_n];
        }
        catch(const std::bad_alloc& e)
        {
            std::cerr << e.what() << std::endl;
            return false;
        }

        in.read(reinterpret_cast<char*>(&m_points[0]), m_n * sizeof(double));
        if(!in.good())
            return false;
        return true;
    }

    virtual void Draw() = 0;
protected:
    // Некоторый набор методов, которым мы можем отрисовывать фигуры
    void drawPoligon(double* points, int size)
    {
        std::cout << "drawPolygon" << std::endl;
        for(int i=0;i<m_n;i++)
            std::cout << m_points[i] << std::endl;
    }
    void drawCircle(double centerX, double centerY, double radius)
    {
        std::cout << "drawCircle" << std::endl;
        for(int i=0;i<m_n;i++)
            std::cout << m_points[i] << std::endl;
    }

protected:
    double* m_points = nullptr;
    int m_n;
};

class Circle : public Shape
{
public:
    Circle() : Shape(3) {}
    virtual ~Circle(){}
    void Draw() override
    {
        drawCircle(m_points[0], m_points[1], m_points[2]);
    }
};

class Triangle : public Shape
{
public:
    Triangle() : Shape(6) {}
    virtual ~Triangle(){}
    void Draw() override
    {
        drawPoligon(m_points, m_n);
    }
};

class Square : public Shape
{
public:
    Square() : Shape(8) {}
    virtual ~Square(){}
    void Draw() override
    {
        drawPoligon(m_points, m_n);
    }
};

std::shared_ptr<Shape> ShapeFactory(uint32_t type)
{
    try {
        switch (type) {
            case eCircle:
                return std::make_shared<Circle>();
            case eTriangle:
                return std::make_shared<Triangle>();
            case eSquare:
                return std::make_shared<Square>();
            default:
                return nullptr;
        }
    }
    catch(const std::bad_alloc& e)
    {
        std::cerr << e.what() << std::endl;
        return nullptr;
    }
}


class Feature
{
public:
    Feature(){}
    ~Feature(){}

    bool isValid()
    {
        return m_shape != nullptr;
    }

    bool Read(const std::string& filename)
    {
        std::ifstream in("features.dat", std::ios::binary);
        if(!in.is_open())
            return false;

        uint32_t type;
        in.read(reinterpret_cast<char*>(&type), sizeof(uint32_t));
        if(!in.good())
            return false;

        m_shape = ShapeFactory(type);
        if(m_shape != nullptr)
            return m_shape->Read(in);
        return false;
    }

    void Draw()
    {
        m_shape->Draw();
    }

protected:
    std::shared_ptr<Shape> m_shape = nullptr;
};

int main()
{
    // Генератор файла для проверки
    std::ofstream out("features.dat", std::ios::binary);
    uint32_t type = 3;
    double mas[8] = {1.7,2.2,3.5, 1, 2, 3, 1, 2};
    out.write(reinterpret_cast<char*>(&type), sizeof(uint32_t));
    out.write(reinterpret_cast<char*>(&mas[0]), 8*sizeof(double));
    out.close();


    Feature feature;
    // Либо использовать исключение
    if(!feature.Read("features.dat"))
        return 2;
    if(!feature.isValid())
        return 1;

    feature.Draw();

    return 0;
}